package tasks.integration;

import javafx.collections.FXCollections;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import tasks.model.Task;
import tasks.model.TasksOperations;
import tasks.model.taskList.ArrayTaskList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ServiceRepositoryEntityTest {
    private long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;
    private Random rand = new Random();

    Task _task1;

    Date date;

    ArrayTaskList _repo;

    TasksOperations _service;

    public void setupListWithTasks() throws ParseException {
        _task1 = new Task("Task1", new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2020"), new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2020"), 10);
        _task1.setActive(true);
        date = new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2020");

        _repo = new ArrayTaskList();
        _repo.add(_task1);
        _service = new TasksOperations(FXCollections.observableArrayList(_repo.getAll()));
    }

    public void setupEmptyList(){
        _repo = new ArrayTaskList();
        _service = new TasksOperations(FXCollections.observableArrayList(_repo.getAll()));
    }

    @Test
    public void whenAreIncomingTasks_shouldReturnCorrectList() throws ParseException{
        setupListWithTasks();
        ArrayList<Task> expectedResult = new ArrayList<Task>();
        expectedResult.add(_task1);
        ArrayList<Task> result = _service.incoming(new Date(date.getTime()-MILLIS_IN_A_DAY), date);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void whenAreNotIncomingTasks_shouldReturnEmptyList(){
        setupEmptyList();
        ArrayList<Task> expectedResult = new ArrayList<Task>();

        ArrayList<Task> result = _service.incoming(date, date);

        Assertions.assertEquals(expectedResult, result);
    }

}
