package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

class TaskTest {

    public static final String EXPECTED_TITLE = "New task";
    public static final int EXPECTED_INTERVAL = 10;
    public static final String EXPECTED_START = "2021-02-12 10:10";
    public static final String EXPECTED_END = "2021-02-12 10:10";
    public static final boolean EXPECTED_ACTIVE = true;

    private Task task;

    @BeforeEach
    void setUp() {

    }

    @Test
    void testTaskCreation_WithSuccess() {
        try {
            task = new Task("New task", Task.getDateFormat().parse("2021-02-12 10:10"), Task.getDateFormat().parse("2021-02-12 10:10"), 10);
            task.setActive(true);

            Assertions.assertEquals(EXPECTED_TITLE, task.getTitle());
            Assertions.assertEquals(EXPECTED_INTERVAL, task.getRepeatInterval());
            Assertions.assertEquals(EXPECTED_END, task.getFormattedDateEnd());
            Assertions.assertEquals(EXPECTED_START, task.getFormattedDateStart());
            Assertions.assertEquals(EXPECTED_ACTIVE, task.isActive());

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Test
    void testTaskCreationIntervalInvalid_ShouldThrowIllegalArgumentException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            try {
                task = new Task("New task", Task.getDateFormat().parse("2021-02-12 10:10"), Task.getDateFormat().parse("2021-02-12 10:10"), 0);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    void testTaskCreationTimeBelowBound_ShouldThrowIllegalArgumentException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            try {
                task = new Task("New task", Task.getDateFormat().parse("1969-02-12 10:10"), Task.getDateFormat().parse("2021-02-12 10:10"), 10);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });
    }

    @AfterEach
    void tearDown() {
    }
}