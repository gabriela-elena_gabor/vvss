package tasks.services;

import org.junit.BeforeClass;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tasks.model.taskList.ArrayTaskList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TasksServiceTest {

    private ArrayTaskList arrayTaskList = new ArrayTaskList();
    private TasksService service = new TasksService(arrayTaskList);
    private String hours = "";
    private String minutes = "";
    private int result;

    @BeforeClass
    public static void m1() {
        System.out.println("Test parseFromStringToSeconds method");
    }

    @BeforeEach
    public void setup() {
        hours = "12";
        minutes = "30";
        result = (12 * DateService.MINUTES_IN_HOUR + 30) * DateService.SECONDS_IN_MINUTE;
    }

    @Tag("ECP")
    @Test
    void whenHoursAndMinutesAreOk_ShouldReturnCorrectParsing() throws Exception {
        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("ECP")
    @ParameterizedTest
    @ValueSource(strings = {"180", "-5"})
    void whenHoursAreNotValid_ShouldThrowIllegalArgumentException(String phours) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            service.parseFromStringToSeconds(phours, minutes);
        });
    }

    @Tag("ECP")
    @Test
    void whenMinutesAreGreaterThen59_ShouldThrowIllegalArgumentException() {
        minutes = "70";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            service.parseFromStringToSeconds(hours, minutes);
        });
    }

    @Tag("ECP")
    @Test
    void whenMinutesAreLessThen0_ShouldThrowIllegalArgumentException() {
        minutes = "-5";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            service.parseFromStringToSeconds(hours, minutes);
        });
    }

    @Tag("BVA")
    @Test
    void whenHoursAreBelowInferiorLimit_ShouldThrowIllegalArgumentException() throws Exception {
        hours = "-1";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            service.parseFromStringToSeconds(hours, minutes);
        });
    }

    @Tag("BVA")
    @Test
    void whenHoursAreAboveInferiorLimit_ShouldReturnCorrectParsing() throws Exception {
        hours = "1";
        result = (1 * DateService.MINUTES_IN_HOUR + 30) * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("BVA")
    @Test
    void whenHoursAreEqualWithInferiorLimit_ShouldReturnCorrectParsing() throws Exception {
        hours = "0";
        result = 30 * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("BVA")
    @Test
    void whenHoursAreAboveSuperiorLimit_ShouldThrowIllegalArgumentException() throws Exception {
        hours = "170";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            service.parseFromStringToSeconds(hours, minutes);
        });
    }

    @Tag("BVA")
    @Test
    void whenHoursAreBelowSuperiorLimit_ShouldReturnCorrectParsing() throws Exception {
        hours = "168";
        result = (168 * DateService.MINUTES_IN_HOUR + 30) * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("BVA")
    @Test
    void whenHoursAreEqualWithSuperiorLimit_ShouldReturnCorrectParsing() throws Exception {
        hours = "168";
        result = (168 * DateService.MINUTES_IN_HOUR + 30) * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("BVA")
    @Test
    void whenMinutesAreBelowInferiorLimit_ShouldThrowIllegalArgumentException() throws Exception {
        minutes = "-1";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            service.parseFromStringToSeconds(hours, minutes);
        });
    }

    @Tag("BVA")
    @Test
    void whenMinutesAreAboveInferiorLimit_ShouldReturnCorrectParsing() throws Exception {
        minutes = "1";
        result = (12 * DateService.MINUTES_IN_HOUR + 1) * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("BVA")
    @Test
    void whenMinutesAreEqualWithInferiorLimit_ShouldReturnCorrectParsing() throws Exception {
        minutes = "0";
        result = 12 * DateService.MINUTES_IN_HOUR * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("BVA")
    @Test
    void whenMinutesAreAboveSuperiorLimit_ShouldThrowIllegalArgumentException() throws Exception {
        minutes = "60";

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            service.parseFromStringToSeconds(hours, minutes);
        });
    }

    @Tag("BVA")
    @Test
    void whenMinutesAreBelowSuperiorLimit_ShouldReturnCorrectParsing() throws Exception {
        minutes = "58";
        result = (12 * DateService.MINUTES_IN_HOUR + 58) * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }

    @Tag("BVA")
    @Test
    void whenMinutesAreEqualWithSuperiorLimit_ShouldReturnCorrectParsing() throws Exception {
        minutes = "59";
        result = (12 * DateService.MINUTES_IN_HOUR + 59) * DateService.SECONDS_IN_MINUTE;

        int resultFromMethod = service.parseFromStringToSeconds(hours, minutes);

        Assertions.assertEquals(result, resultFromMethod);
    }
}