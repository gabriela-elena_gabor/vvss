package tasks.services;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import tasks.model.Task;
import tasks.model.TasksOperations;
import tasks.model.taskList.ArrayTaskList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TasksOperationsTest {

    private ArrayTaskList _arrayTaskList = null;
    private TasksOperations _tasksOperations = null;
    Task _task1 = null;
    Task _task2 = null;
    Task _task3 = null;

    @BeforeEach
    public void setup() throws ParseException {
        _task1 = new Task("Task1", new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2020"), new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2020"), 10);
        _task2 = new Task("Task2", new SimpleDateFormat("dd/MM/yyyy").parse("01/03/2021"), new SimpleDateFormat("dd/MM/yyyy").parse("15/03/2021"), 10);
        _task3 = new Task("Task3", new SimpleDateFormat("dd/MM/yyyy").parse("01/05/2022"), new SimpleDateFormat("dd/MM/yyyy").parse("01/05/2022"), 10);

        _task1.setActive(true);
        _task2.setActive(true);
        _task3.setActive(true);

        _arrayTaskList = new ArrayTaskList();
        _arrayTaskList.add(_task1);
        _arrayTaskList.add(_task2);
        _arrayTaskList.add(_task3);

        _tasksOperations = new TasksOperations(FXCollections.observableArrayList(_arrayTaskList.getAll()));
    }

    @Tag("TC01")
    @Test
    void whenTasksListIsEmpty_ShouldReturnEmptyList() throws Exception {
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("22/11/2020");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("23/11/2020");
        _arrayTaskList = new ArrayTaskList();
        _tasksOperations = new TasksOperations(FXCollections.observableArrayList(_arrayTaskList.getAll()));
        ArrayList<Task> expectedResult = new ArrayList<Task>();

        ArrayList<Task> resultFromMethod = _tasksOperations.incoming(startDate, endDate);

        Assertions.assertEquals(expectedResult, resultFromMethod);
    }

    @Tag("TC04")
    @Test
    void whenThereAreNoTasksInTimeFrame_ShouldReturnEmptyList() throws Exception {
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("22/11/2020");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("23/11/2020");
        ArrayList<Task> expectedResult = new ArrayList<Task>();

        ArrayList<Task> resultFromMethod = _tasksOperations.incoming(startDate, endDate);

        Assertions.assertEquals(expectedResult, resultFromMethod);
    }

    @Tag("TC02")
    @Test
    void whenThereAreTasksWithEndDateBeforeToEndParameter_ShouldReturnCorrectList() throws Exception {
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/04/2021");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("31/04/2022");
        ArrayList<Task> expectedResult = new ArrayList<Task>();
        expectedResult.add(_task3);

        ArrayList<Task> resultFromMethod = _tasksOperations.incoming(startDate, endDate);

        Assertions.assertEquals(expectedResult, resultFromMethod);
    }

    @Tag("TC04")
    @Test
    void whenThereAreTasksWithEndDateAfterEndParameter_ShouldReturnCorrectList() throws Exception {
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/04/2021");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("15/04/2022");
        ArrayList<Task> expectedResult = new ArrayList<Task>();

        ArrayList<Task> resultFromMethod = _tasksOperations.incoming(startDate, endDate);

        Assertions.assertEquals(expectedResult, resultFromMethod);
    }

    @Tag("TC03")
    @Test
    void whenThereAreTasksWithEndDateEqualsToEndParameter_ShouldReturnCorrectList() throws Exception {
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/04/2021");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/05/2022");
        ArrayList<Task> expectedResult = new ArrayList<Task>();
        expectedResult.add(_task3);

        ArrayList<Task> resultFromMethod = _tasksOperations.incoming(startDate, endDate);

        Assertions.assertEquals(expectedResult, resultFromMethod);
    }

    @Tag("TC05")
    @Test
    void whenNextTimeAfterIsNull_ShouldReturnEmptyList() throws Exception {
        Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("22/11/2020");
        Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("22/11/2020");
        ArrayList<Task> expectedResult = new ArrayList<Task>();

        ArrayList<Task> resultFromMethod = _tasksOperations.incoming(startDate, endDate);

        Assertions.assertEquals(expectedResult, resultFromMethod);
    }
}