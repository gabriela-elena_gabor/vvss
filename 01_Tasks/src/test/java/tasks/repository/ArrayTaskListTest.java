package tasks.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;
import tasks.model.taskList.ArrayTaskList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ArrayTaskListTest {

    private final List<Task> tasks = new ArrayList<>();
    private ArrayTaskList arrayTaskList;

    @BeforeEach
    public void setup() {
    }

    @Test
    void testTaskGetAll() throws ParseException {
        arrayTaskList = new ArrayTaskList();
        Task _task1 = new Task("Task1", new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2020"), new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2020"), 10);
        Task _task2 = new Task("Task2", new SimpleDateFormat("dd/MM/yyyy").parse("01/03/2021"), new SimpleDateFormat("dd/MM/yyyy").parse("15/03/2021"), 10);
        Task _task3 = new Task("Task3", new SimpleDateFormat("dd/MM/yyyy").parse("01/05/2022"), new SimpleDateFormat("dd/MM/yyyy").parse("01/05/2022"), 10);

        tasks.add(_task1);
        tasks.add(_task2);
        tasks.add(_task3);

        arrayTaskList.add(_task1);
        arrayTaskList.add(_task2);
        arrayTaskList.add(_task3);

        Assertions.assertEquals(arrayTaskList.getAll(), tasks);
    }

    @Test
    void testTaskGetAll_emptyList() {
        arrayTaskList = new ArrayTaskList();
        Assertions.assertEquals(arrayTaskList.getAll(), tasks);
    }

}
