describe('Login Valid', () => {
  it('finds the content "type"', () => {
    cy.visit('http://localhost:4200/login')


	
 
    cy.fixture('login_valid.json').then((user)=>{
		cy.get('#username').type(user.username, { delay: 100 }).should('have.value',user.username)
		cy.get('#password').type(user.password, { delay: 100 }).should('have.value',user.password)
	})


		
	cy.get('#login-form').click()
	
	cy.url().should('eq', 'http://localhost:4200/home')


  })
}) 