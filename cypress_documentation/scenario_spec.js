describe('Scenario', () => {
  it('finds the content "type"', () => {
    cy.visit('http://localhost:4200/login')


    cy.fixture('login_valid.json').then((user)=>{
		cy.get('#username').type(user.username, { delay: 100 }).should('have.value',user.username)
		cy.get('#password').type(user.password, { delay: 100 }).should('have.value',user.password)
	})


	cy.get('#login-form').click()
	cy.url().should('eq', 'http://localhost:4200/home')
	cy.wait(2000)
	
	
	cy.get('#profile-button').click()
	cy.url().should('eq', 'http://localhost:4200/profile')
	 cy.fixture('profile.json').then((profile)=>{
		cy.get('#name').type(profile.name)
	
	})
	cy.wait(1000)
	
	cy.get('#save-profile').click()
	cy.url().should('eq', 'http://localhost:4200/home')
	cy.wait(2000)
	
	
	cy.get('#myReports-button').click()
	cy.url().should('eq', 'http://localhost:4200/myReports')
	cy.wait(2000)
	
	cy.get('#report-button').click()
	cy.url().should('eq', 'http://localhost:4200/report')
	cy.fixture('report_problem.json').then((problem)=>{
		cy.get('#numberOfRoom').type(problem.numberOfRoom, { delay: 100 }).should('have.value',problem.numberOfRoom)
		cy.get('#content').type(problem.content, { delay: 100 }).should('have.value',problem.content)
		cy.get('select').select(problem.domain)
	})
	cy.wait(2000)
	cy.get('#report-form').click()
	cy.url().should('eq', 'http://localhost:4200/home')
	cy.wait(2000)
	
	cy.get('#myReports-button').click()
	cy.url().should('eq', 'http://localhost:4200/myReports')
	cy.wait(2000)
	
	cy.get('#logout-button').click()
	
	cy.url().should('eq', 'http://localhost:4200/login')

  })

}) 